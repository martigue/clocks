package clock;

import java.util.Calendar;

import util.RangedNumber;

/**
 * A very simple model of a clock that stores hours, minutes, and seconds. It stores hours between 1-12. The clock
 * doesn't "move". In other words, it does not keep time. It only stores a time. It must be updated by an external
 * force.
 * 
 * @author Chuck Cusack, January 2013 (Moved from the ClockViewClass)
 */
public class ClockModel implements ClockInterface {
	private RangedNumber	second;
	private RangedNumber	minute;
	private RangedNumber	hour;

	/**
	 * Create a clock with the current time.
	 */
	public ClockModel() {
		second = new RangedNumber(60);
		minute = new RangedNumber(60);
		hour = new RangedNumber(12);
		Calendar now = Calendar.getInstance();
		setSeconds(now.get(Calendar.SECOND));
		setMinutes(now.get(Calendar.MINUTE));
		setHours(now.get(Calendar.HOUR));
	}

	/**
	 * Create a clock with the given time.
	 * 
	 * @param second desired seconds
	 * @param minute desired minutes
	 * @param hour desired hours
	 */
	public ClockModel(int hour, int minute, int second) {
		super();
		this.second = new RangedNumber(60, second);
		this.minute = new RangedNumber(60, minute);
		this.hour = new RangedNumber(12, hour);
	}

	/*
	 * The typical getters that you would expect.
	 */
	/* (non-Javadoc)
	 * @see clock.ClockInterface#getSeconds()
	 */
	@Override
	public int getSeconds() {
		return second.getNumber();
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#getMinutes()
	 */
	@Override
	public int getMinutes() {
		return minute.getNumber();
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#getHours()
	 */
	@Override
	public int getHours() {
		// We have to replace 0 with 12 since the
		// RangedNumber doesn't know the context and that
		// we want 0 to be 12.
		int h = hour.getNumber();
		if (h == 0)
			return 12;
		else
			return h;
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#setTime(int, int, int)
	 */
	@Override
	public void setTime(int hr, int min, int sec) {
		hour.setNumber(hr);
		minute.setNumber(min);
		second.setNumber(sec);
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#setSeconds(int)
	 */
	@Override
	public void setSeconds(int sec) {
		second.setNumber(sec);
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#incrementSeconds()
	 */
	@Override
	public void incrementSeconds() {
		boolean wrapped = second.increment();
		if (wrapped) {
			incrementMinutes();
		}
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#setMinutes(int)
	 */
	@Override
	public void setMinutes(int min) {
		minute.setNumber(min);
	}
	
	/* (non-Javadoc)
	 * @see clock.ClockInterface#incrementMinutes()
	 */
	@Override
	public void incrementMinutes() {
		boolean wrapped = minute.increment();
		if (wrapped) {
			incrementHours();
		}
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#setHours(int)
	 */
	@Override
	public void setHours(int hr) {
		hour.setNumber(hr);
	}

	/* (non-Javadoc)
	 * @see clock.ClockInterface#incrementHours()
	 */
	@Override
	public void incrementHours() {
		hour.increment();
	}

}
